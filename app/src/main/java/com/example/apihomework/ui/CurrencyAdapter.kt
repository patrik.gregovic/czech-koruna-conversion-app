package com.example.apihomework.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apihomework.R
import com.example.apihomework.data.model.Currency
import com.example.apihomework.databinding.CurrencyItemBinding

class CurrencyAdapter(): RecyclerView.Adapter<CurrencyAdapter.CurrencyItem>() {

    private var data: ArrayList<Currency> = arrayListOf()

    class CurrencyItem(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = CurrencyItemBinding.bind(itemView)

        fun configure(currency: Currency) {
            binding.tvTag.text = currency.tag
            binding.tvAmount.text = currency.amount.toString()
            binding.tvRate.text = currency.rate.toString()
        }
    }

    fun updateData(currencyData: ArrayList<Currency>) {
        data = currencyData
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyItem {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.currency_item, parent, false)
        return CurrencyItem(itemView)
    }

    override fun onBindViewHolder(holder: CurrencyItem, position: Int) {
        val currency = data[position]
        holder.configure(currency)
    }
}
