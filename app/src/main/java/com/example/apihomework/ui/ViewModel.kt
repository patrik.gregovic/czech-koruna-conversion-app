package com.example.apihomework.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apihomework.data.model.Currency
import com.example.apihomework.data.repository.CurrencyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ViewModel: ViewModel() {

    private val repo = CurrencyRepository()

    val currencies:  MutableLiveData<ArrayList<Currency>> = MutableLiveData(ArrayList())

    fun getData(date: String, isMain: Boolean) {
        viewModelScope.launch(Dispatchers.IO){

                if(isMain){ //for main CNB currencies - get plain/text response with delimiters
                    val response = repo.getCurrencies(date)
                    val responseRows: List<String> = response.split("\n")
                    var currenciesResponse: ArrayList<Currency>? = ArrayList()

                    for(row in responseRows){
                        if(row.isNotEmpty()) {
                            if (!row.first().equals('z') && !row.first().isDigit()) {
                                val rowItems: List<String> = row.split("|")
                                val tag: String = rowItems[3]
                                val amount: Int = rowItems[2].toInt()
                                val rate: Float = rowItems[4].replace(',', '.').toFloat()

                                currenciesResponse?.add(Currency(tag,amount,rate))
                            }
                        }
                    }

                    currencies.postValue(currenciesResponse)
                }
                else { //else for other currencies the API returns a text/HTML which we must scrape
                    var stringToDate: Date = SimpleDateFormat("dd.MM.yyyy").parse(date)
                    val cal = Calendar.getInstance()
                    cal.time = stringToDate
                    val month: Int = cal.get(Calendar.MONTH)+1//add one because this format uses months from 0-11
                    val year: Int = cal.get(Calendar.YEAR)
                    val response = repo.getOtherCurrencies(month, year)
                    val doc: Document = Jsoup.parse(response)

                    val tds: Elements = doc.select("tbody > tr")

                    var currenciesResponse: ArrayList<Currency>? = ArrayList()
                    for(i in 1..tds.size ){
                        val tr = tds.select(":nth-child($i) td")
                        val tag: String = tr.select(":nth-child(4)").text()
                        val amount: Int = tr.select(":nth-child(3)").text().toInt()
                        val rate: Float = tr.select(":nth-child(5)").text().replace(',', '.').toFloat()
                        currenciesResponse?.add(Currency(tag,amount,rate))
                    }
                    currencies.postValue(currenciesResponse)
                }

                print(currencies.value)
            }
    }
}