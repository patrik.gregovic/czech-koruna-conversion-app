package com.example.apihomework.ui

import android.os.Bundle
import android.view.View
import android.widget.CalendarView.OnDateChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.apihomework.R
import com.example.apihomework.databinding.FragmentChoicesBinding
import java.text.SimpleDateFormat
import java.util.*


class ChoicesFragment : Fragment(R.layout.fragment_choices) {

    private lateinit var binding: FragmentChoicesBinding

    private val viewModel: ViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title  = "CNB Currency Rates"

        binding = FragmentChoicesBinding.bind(view)

        binding.calendarPicker.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth
            view.date = calendar.timeInMillis
            println(view.date.toString())
        }

        binding.submitBtn.setOnClickListener {
            val date = binding.calendarPicker.date
            val dateString = SimpleDateFormat("dd.MM.yyyy").format(date)
            //println(dateString)

            val mainCountries = binding.countriesToggle.isChecked

            val title = if (mainCountries) "Main Countries Rates (Weekly)" else "Other Countries Rates (Monthly)"

            //set direction
            val direction =
                ChoicesFragmentDirections.actionChoicesFragmentToResultsFragment(
                    title,
                    dateString,
                    mainCountries
                )
            // navigate using direction
            findNavController().navigate(direction)
        }
    }

}