package com.example.apihomework.ui

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearSnapHelper
import com.example.apihomework.R
import com.example.apihomework.databinding.FragmentResultsBinding
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

class ResultsFragment : Fragment(R.layout.fragment_results) {

    private lateinit var binding: FragmentResultsBinding

    private val args: ResultsFragmentArgs by navArgs()

    private val viewModel: ViewModel by viewModels()

    private val currencyAdapter = CurrencyAdapter()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.title  = args.title
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentResultsBinding.bind(view)

        binding.currencyRecyclerView.adapter = currencyAdapter

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.currencyRecyclerView)

        viewModel.currencies.observe(viewLifecycleOwner, Observer { data ->
                currencyAdapter.updateData(data)
                val tags = data.joinToString("\n") { it.tag }
                println(data)
                println(viewModel.currencies.value)

        })

        viewModel.getData(args.dateString, args.isMain)
    }

}