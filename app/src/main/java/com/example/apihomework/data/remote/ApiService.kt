package com.example.apihomework.data.remote

import com.example.apihomework.data.model.Currency
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt")
    suspend fun getCurrencies(@Query(value="date") date:String?): String

    @GET("kurzy-ostatnich-men/kurzy-ostatnich-men/")
    suspend fun getOtherCurrencies(@Query(value="mesic") month:Int?, @Query(value="rok") year:Int?): String

    companion object {
        const val baseUrl = "https://www.cnb.cz/cs/financni-trhy/devizovy-trh/"

        val client = Retrofit
            .Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

}