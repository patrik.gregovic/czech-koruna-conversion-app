package com.example.apihomework.data.repository

import com.example.apihomework.data.remote.ApiService

class CurrencyRepository (
    private val service: ApiService = ApiService.client
) {
    suspend fun getCurrencies(date:String?) = service.getCurrencies(date)

    suspend fun getOtherCurrencies(month:Int?, year:Int?) = service.getOtherCurrencies(month,year)
}