package com.example.apihomework.data.model

data class Currency(
    val tag: String,
    val amount: Int,
    val rate: Float
)